const middleware = {}

middleware['admin'] = require('../middleware/admin.js')
middleware['admin'] = middleware['admin'].default || middleware['admin']

middleware['auth'] = require('../middleware/auth.js')
middleware['auth'] = middleware['auth'].default || middleware['auth']

middleware['baak'] = require('../middleware/baak.js')
middleware['baak'] = middleware['baak'].default || middleware['baak']

middleware['dosen'] = require('../middleware/dosen.js')
middleware['dosen'] = middleware['dosen'].default || middleware['dosen']

middleware['kaprodi'] = require('../middleware/kaprodi.js')
middleware['kaprodi'] = middleware['kaprodi'].default || middleware['kaprodi']

middleware['mahasiswa'] = require('../middleware/mahasiswa.js')
middleware['mahasiswa'] = middleware['mahasiswa'].default || middleware['mahasiswa']

export default middleware
