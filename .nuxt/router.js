import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from 'ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _272271c4 = () => interopDefault(import('../pages/admin/beranda.vue' /* webpackChunkName: "pages/admin/beranda" */))
const _66c12e4b = () => interopDefault(import('../pages/admin/jadwal-kuliah.vue' /* webpackChunkName: "pages/admin/jadwal-kuliah" */))
const _90cea2ba = () => interopDefault(import('../pages/admin/rekap-presensi/index.vue' /* webpackChunkName: "pages/admin/rekap-presensi/index" */))
const _04a5ecf7 = () => interopDefault(import('../pages/admin/support/index.vue' /* webpackChunkName: "pages/admin/support/index" */))
const _acc00684 = () => interopDefault(import('../pages/auth/dosen-lb.vue' /* webpackChunkName: "pages/auth/dosen-lb" */))
const _45eb413e = () => interopDefault(import('../pages/baak/beranda.vue' /* webpackChunkName: "pages/baak/beranda" */))
const _0841906b = () => interopDefault(import('../pages/baak/jadwal-kuliah.vue' /* webpackChunkName: "pages/baak/jadwal-kuliah" */))
const _26ee6496 = () => interopDefault(import('../pages/baak/pengumuman.vue' /* webpackChunkName: "pages/baak/pengumuman" */))
const _d528aefa = () => interopDefault(import('../pages/baak/rekap-presensi/index.vue' /* webpackChunkName: "pages/baak/rekap-presensi/index" */))
const _1bdd373a = () => interopDefault(import('../pages/baak/rekap-tugas/index.vue' /* webpackChunkName: "pages/baak/rekap-tugas/index" */))
const _b3b361d2 = () => interopDefault(import('../pages/baak/support/index.vue' /* webpackChunkName: "pages/baak/support/index" */))
const _235e40c0 = () => interopDefault(import('../pages/dosen/beranda.vue' /* webpackChunkName: "pages/dosen/beranda" */))
const _d922491c = () => interopDefault(import('../pages/dosen/jadwal-online.vue' /* webpackChunkName: "pages/dosen/jadwal-online" */))
const _5df75598 = () => interopDefault(import('../pages/dosen/matakuliah.vue' /* webpackChunkName: "pages/dosen/matakuliah" */))
const _868a555a = () => interopDefault(import('../pages/dosen/materi-perkuliahan/index.vue' /* webpackChunkName: "pages/dosen/materi-perkuliahan/index" */))
const _33911cee = () => interopDefault(import('../pages/dosen/praktikum.vue' /* webpackChunkName: "pages/dosen/praktikum" */))
const _3faf6c41 = () => interopDefault(import('../pages/dosen/rekap-presensi/index.vue' /* webpackChunkName: "pages/dosen/rekap-presensi/index" */))
const _58082519 = () => interopDefault(import('../pages/dosen/support/index.vue' /* webpackChunkName: "pages/dosen/support/index" */))
const _a704c65e = () => interopDefault(import('../pages/dosen/tugas-online.vue' /* webpackChunkName: "pages/dosen/tugas-online" */))
const _5e4f9a31 = () => interopDefault(import('../pages/dosen/uas/index.vue' /* webpackChunkName: "pages/dosen/uas/index" */))
const _1f023f84 = () => interopDefault(import('../pages/dosen/uts/index.vue' /* webpackChunkName: "pages/dosen/uts/index" */))
const _5eb7cd2a = () => interopDefault(import('../pages/kaprodi/beranda.vue' /* webpackChunkName: "pages/kaprodi/beranda" */))
const _0bce4176 = () => interopDefault(import('../pages/kaprodi/rekap-presensi/index.vue' /* webpackChunkName: "pages/kaprodi/rekap-presensi/index" */))
const _78c909b2 = () => interopDefault(import('../pages/kaprodi/rekap-tugas/index.vue' /* webpackChunkName: "pages/kaprodi/rekap-tugas/index" */))
const _7a0fc9bd = () => interopDefault(import('../pages/links/materi.vue' /* webpackChunkName: "pages/links/materi" */))
const _4f8c2507 = () => interopDefault(import('../pages/links/tugas.vue' /* webpackChunkName: "pages/links/tugas" */))
const _016f3b4a = () => interopDefault(import('../pages/links/video.vue' /* webpackChunkName: "pages/links/video" */))
const _bafcc06e = () => interopDefault(import('../pages/mahasiswa/beranda.vue' /* webpackChunkName: "pages/mahasiswa/beranda" */))
const _38670fbb = () => interopDefault(import('../pages/mahasiswa/jadwal-online.vue' /* webpackChunkName: "pages/mahasiswa/jadwal-online" */))
const _d9b0a422 = () => interopDefault(import('../pages/mahasiswa/matakuliah.vue' /* webpackChunkName: "pages/mahasiswa/matakuliah" */))
const _1402496a = () => interopDefault(import('../pages/mahasiswa/materi-perkuliahan/index.vue' /* webpackChunkName: "pages/mahasiswa/materi-perkuliahan/index" */))
const _29ff3252 = () => interopDefault(import('../pages/mahasiswa/praktikum.vue' /* webpackChunkName: "pages/mahasiswa/praktikum" */))
const _05ff4d3c = () => interopDefault(import('../pages/mahasiswa/support/index.vue' /* webpackChunkName: "pages/mahasiswa/support/index" */))
const _56f93b68 = () => interopDefault(import('../pages/mahasiswa/tugas-online.vue' /* webpackChunkName: "pages/mahasiswa/tugas-online" */))
const _bbd14a0c = () => interopDefault(import('../pages/mahasiswa/uas/index.vue' /* webpackChunkName: "pages/mahasiswa/uas/index" */))
const _3446a107 = () => interopDefault(import('../pages/mahasiswa/uts/index.vue' /* webpackChunkName: "pages/mahasiswa/uts/index" */))
const _50f9bfde = () => interopDefault(import('../pages/admin/bantuan/generate-token.vue' /* webpackChunkName: "pages/admin/bantuan/generate-token" */))
const _d9860a60 = () => interopDefault(import('../pages/admin/fcm/example.vue' /* webpackChunkName: "pages/admin/fcm/example" */))
const _9ebdc850 = () => interopDefault(import('../pages/admin/master/mis/index.vue' /* webpackChunkName: "pages/admin/master/mis/index" */))
const _6073207b = () => interopDefault(import('../pages/admin/master/room-meeting.vue' /* webpackChunkName: "pages/admin/master/room-meeting" */))
const _4a6d82a4 = () => interopDefault(import('../pages/admin/master/server-conference.vue' /* webpackChunkName: "pages/admin/master/server-conference" */))
const _604dc348 = () => interopDefault(import('../pages/admin/pengaturan/tanggal-libur.vue' /* webpackChunkName: "pages/admin/pengaturan/tanggal-libur" */))
const _585f9560 = () => interopDefault(import('../pages/admin/rekap-presensi/detail.vue' /* webpackChunkName: "pages/admin/rekap-presensi/detail" */))
const _286ac17c = () => interopDefault(import('../pages/admin/support/detail.vue' /* webpackChunkName: "pages/admin/support/detail" */))
const _211c255e = () => interopDefault(import('../pages/admin/ujian/uas.vue' /* webpackChunkName: "pages/admin/ujian/uas" */))
const _4188392b = () => interopDefault(import('../pages/admin/ujian/uts.vue' /* webpackChunkName: "pages/admin/ujian/uts" */))
const _9f471120 = () => interopDefault(import('../pages/baak/rekap-presensi/detail.vue' /* webpackChunkName: "pages/baak/rekap-presensi/detail" */))
const _0fc680ce = () => interopDefault(import('../pages/baak/rekap-tugas/detail.vue' /* webpackChunkName: "pages/baak/rekap-tugas/detail" */))
const _9212b948 = () => interopDefault(import('../pages/baak/support/detail.vue' /* webpackChunkName: "pages/baak/support/detail" */))
const _0d62977e = () => interopDefault(import('../pages/baak/ujian/uas.vue' /* webpackChunkName: "pages/baak/ujian/uas" */))
const _2dceab4b = () => interopDefault(import('../pages/baak/ujian/uts.vue' /* webpackChunkName: "pages/baak/ujian/uts" */))
const _4545458e = () => interopDefault(import('../pages/dosen/kuliah/detail.vue' /* webpackChunkName: "pages/dosen/kuliah/detail" */))
const _4ba943a8 = () => interopDefault(import('../pages/dosen/kuliah/forum/index.vue' /* webpackChunkName: "pages/dosen/kuliah/forum/index" */))
const _dacb787c = () => interopDefault(import('../pages/dosen/kuliah/mahasiswa.vue' /* webpackChunkName: "pages/dosen/kuliah/mahasiswa" */))
const _05aadf64 = () => interopDefault(import('../pages/dosen/kuliah/materi.vue' /* webpackChunkName: "pages/dosen/kuliah/materi" */))
const _70b0e19a = () => interopDefault(import('../pages/dosen/kuliah/pengumuman.vue' /* webpackChunkName: "pages/dosen/kuliah/pengumuman" */))
const _1e178300 = () => interopDefault(import('../pages/dosen/kuliah/tugas.vue' /* webpackChunkName: "pages/dosen/kuliah/tugas" */))
const _ba51567a = () => interopDefault(import('../pages/dosen/kuliah/video.vue' /* webpackChunkName: "pages/dosen/kuliah/video" */))
const _3b5c796e = () => interopDefault(import('../pages/dosen/materi-perkuliahan/daftar-materi.vue' /* webpackChunkName: "pages/dosen/materi-perkuliahan/daftar-materi" */))
const _4e912b72 = () => interopDefault(import('../pages/dosen/rekap-presensi/detail.vue' /* webpackChunkName: "pages/dosen/rekap-presensi/detail" */))
const _577e1acf = () => interopDefault(import('../pages/dosen/support/buat.vue' /* webpackChunkName: "pages/dosen/support/buat" */))
const _414f8d9a = () => interopDefault(import('../pages/dosen/support/detail.vue' /* webpackChunkName: "pages/dosen/support/detail" */))
const _03f6bb82 = () => interopDefault(import('../pages/dosen/uas/detail.vue' /* webpackChunkName: "pages/dosen/uas/detail" */))
const _909f91d6 = () => interopDefault(import('../pages/dosen/uts/detail.vue' /* webpackChunkName: "pages/dosen/uts/detail" */))
const _064cfcdd = () => interopDefault(import('../pages/kaprodi/rekap-presensi/detail.vue' /* webpackChunkName: "pages/kaprodi/rekap-presensi/detail" */))
const _4826f94c = () => interopDefault(import('../pages/kaprodi/rekap-tugas/detail.vue' /* webpackChunkName: "pages/kaprodi/rekap-tugas/detail" */))
const _fb54dcfc = () => interopDefault(import('../pages/mahasiswa/kuliah/detail.vue' /* webpackChunkName: "pages/mahasiswa/kuliah/detail" */))
const _6e7520fa = () => interopDefault(import('../pages/mahasiswa/kuliah/forum/index.vue' /* webpackChunkName: "pages/mahasiswa/kuliah/forum/index" */))
const _4266bed9 = () => interopDefault(import('../pages/mahasiswa/kuliah/mahasiswa.vue' /* webpackChunkName: "pages/mahasiswa/kuliah/mahasiswa" */))
const _aab9d8a6 = () => interopDefault(import('../pages/mahasiswa/kuliah/materi.vue' /* webpackChunkName: "pages/mahasiswa/kuliah/materi" */))
const _116a76fc = () => interopDefault(import('../pages/mahasiswa/kuliah/pengumuman.vue' /* webpackChunkName: "pages/mahasiswa/kuliah/pengumuman" */))
const _c92045d2 = () => interopDefault(import('../pages/mahasiswa/kuliah/tugas.vue' /* webpackChunkName: "pages/mahasiswa/kuliah/tugas" */))
const _4d52f35a = () => interopDefault(import('../pages/mahasiswa/kuliah/video.vue' /* webpackChunkName: "pages/mahasiswa/kuliah/video" */))
const _9fd432f6 = () => interopDefault(import('../pages/mahasiswa/materi-perkuliahan/daftar-materi.vue' /* webpackChunkName: "pages/mahasiswa/materi-perkuliahan/daftar-materi" */))
const _01f9b966 = () => interopDefault(import('../pages/mahasiswa/support/buat.vue' /* webpackChunkName: "pages/mahasiswa/support/buat" */))
const _3b5de271 = () => interopDefault(import('../pages/mahasiswa/support/detail.vue' /* webpackChunkName: "pages/mahasiswa/support/detail" */))
const _392713d9 = () => interopDefault(import('../pages/mahasiswa/uas/detail.vue' /* webpackChunkName: "pages/mahasiswa/uas/detail" */))
const _2503e6a6 = () => interopDefault(import('../pages/mahasiswa/uas/detail-ujian.vue' /* webpackChunkName: "pages/mahasiswa/uas/detail-ujian" */))
const _263ee128 = () => interopDefault(import('../pages/mahasiswa/uts/detail.vue' /* webpackChunkName: "pages/mahasiswa/uts/detail" */))
const _31234e00 = () => interopDefault(import('../pages/mahasiswa/uts/detail-ujian.vue' /* webpackChunkName: "pages/mahasiswa/uts/detail-ujian" */))
const _b5f53f56 = () => interopDefault(import('../pages/admin/master/mis/agama.vue' /* webpackChunkName: "pages/admin/master/mis/agama" */))
const _6764c74c = () => interopDefault(import('../pages/admin/master/mis/hari.vue' /* webpackChunkName: "pages/admin/master/mis/hari" */))
const _25b0ce9c = () => interopDefault(import('../pages/admin/master/mis/jam.vue' /* webpackChunkName: "pages/admin/master/mis/jam" */))
const _7581697f = () => interopDefault(import('../pages/admin/master/mis/jam-pjj.vue' /* webpackChunkName: "pages/admin/master/mis/jam-pjj" */))
const _2788770c = () => interopDefault(import('../pages/admin/master/mis/jam-psdku.vue' /* webpackChunkName: "pages/admin/master/mis/jam-psdku" */))
const _31b61f32 = () => interopDefault(import('../pages/admin/master/mis/jam-reguler.vue' /* webpackChunkName: "pages/admin/master/mis/jam-reguler" */))
const _37787bd8 = () => interopDefault(import('../pages/admin/master/mis/jurusan.vue' /* webpackChunkName: "pages/admin/master/mis/jurusan" */))
const _5b512fec = () => interopDefault(import('../pages/admin/master/mis/kelas.vue' /* webpackChunkName: "pages/admin/master/mis/kelas" */))
const _4867c6b8 = () => interopDefault(import('../pages/admin/master/mis/kuliah.vue' /* webpackChunkName: "pages/admin/master/mis/kuliah" */))
const _5613c57a = () => interopDefault(import('../pages/admin/master/mis/kuliah-agama.vue' /* webpackChunkName: "pages/admin/master/mis/kuliah-agama" */))
const _470ae9e7 = () => interopDefault(import('../pages/admin/master/mis/kuliah-agama-mahasiswa.vue' /* webpackChunkName: "pages/admin/master/mis/kuliah-agama-mahasiswa" */))
const _0b4a06b8 = () => interopDefault(import('../pages/admin/master/mis/kuliah-pararel.vue' /* webpackChunkName: "pages/admin/master/mis/kuliah-pararel" */))
const _4917ab00 = () => interopDefault(import('../pages/admin/master/mis/mahasiswa.vue' /* webpackChunkName: "pages/admin/master/mis/mahasiswa" */))
const _31f18915 = () => interopDefault(import('../pages/admin/master/mis/mahasiswa-semester.vue' /* webpackChunkName: "pages/admin/master/mis/mahasiswa-semester" */))
const _6327710e = () => interopDefault(import('../pages/admin/master/mis/matakuliah.vue' /* webpackChunkName: "pages/admin/master/mis/matakuliah" */))
const _71f06914 = () => interopDefault(import('../pages/admin/master/mis/pegawai.vue' /* webpackChunkName: "pages/admin/master/mis/pegawai" */))
const _37e6450a = () => interopDefault(import('../pages/admin/master/mis/program.vue' /* webpackChunkName: "pages/admin/master/mis/program" */))
const _a7a177e4 = () => interopDefault(import('../pages/admin/master/mis/ruang-kuliah.vue' /* webpackChunkName: "pages/admin/master/mis/ruang-kuliah" */))
const _f61a6232 = () => interopDefault(import('../pages/admin/master/mis/skema.vue' /* webpackChunkName: "pages/admin/master/mis/skema" */))
const _0ab9ab71 = () => interopDefault(import('../pages/admin/master/mis/soal.vue' /* webpackChunkName: "pages/admin/master/mis/soal" */))
const _c269091a = () => interopDefault(import('../pages/admin/master/mis/soal-agama.vue' /* webpackChunkName: "pages/admin/master/mis/soal-agama" */))
const _f97d4238 = () => interopDefault(import('../pages/dosen/kuliah/rekap-nilai/tugas.vue' /* webpackChunkName: "pages/dosen/kuliah/rekap-nilai/tugas" */))
const _2198a313 = () => interopDefault(import('../pages/dosen/kuliah/rekap-nilai/uas.vue' /* webpackChunkName: "pages/dosen/kuliah/rekap-nilai/uas" */))
const _4204b6e0 = () => interopDefault(import('../pages/dosen/kuliah/rekap-nilai/uts.vue' /* webpackChunkName: "pages/dosen/kuliah/rekap-nilai/uts" */))
const _ed1cbb6a = () => interopDefault(import('../pages/dosen/notifikasi/presensi/_id.vue' /* webpackChunkName: "pages/dosen/notifikasi/presensi/_id" */))
const _389fdcfc = () => interopDefault(import('../pages/dosen/notifikasi/tugas/_id.vue' /* webpackChunkName: "pages/dosen/notifikasi/tugas/_id" */))
const _0df2c83e = () => interopDefault(import('../pages/mahasiswa/notifikasi/materi/_id.vue' /* webpackChunkName: "pages/mahasiswa/notifikasi/materi/_id" */))
const _41efcf54 = () => interopDefault(import('../pages/mahasiswa/notifikasi/presensi/_id.vue' /* webpackChunkName: "pages/mahasiswa/notifikasi/presensi/_id" */))
const _d7e9adce = () => interopDefault(import('../pages/mahasiswa/notifikasi/tugas/_id.vue' /* webpackChunkName: "pages/mahasiswa/notifikasi/tugas/_id" */))
const _53b93448 = () => interopDefault(import('../pages/mahasiswa/notifikasi/video/_id.vue' /* webpackChunkName: "pages/mahasiswa/notifikasi/video/_id" */))
const _d3350e92 = () => interopDefault(import('../pages/index.vue' /* webpackChunkName: "pages/index" */))

const emptyFn = () => {}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/admin/beranda",
    component: _272271c4,
    name: "admin-beranda"
  }, {
    path: "/admin/jadwal-kuliah",
    component: _66c12e4b,
    name: "admin-jadwal-kuliah"
  }, {
    path: "/admin/rekap-presensi",
    component: _90cea2ba,
    name: "admin-rekap-presensi"
  }, {
    path: "/admin/support",
    component: _04a5ecf7,
    name: "admin-support"
  }, {
    path: "/auth/dosen-lb",
    component: _acc00684,
    name: "auth-dosen-lb"
  }, {
    path: "/baak/beranda",
    component: _45eb413e,
    name: "baak-beranda"
  }, {
    path: "/baak/jadwal-kuliah",
    component: _0841906b,
    name: "baak-jadwal-kuliah"
  }, {
    path: "/baak/pengumuman",
    component: _26ee6496,
    name: "baak-pengumuman"
  }, {
    path: "/baak/rekap-presensi",
    component: _d528aefa,
    name: "baak-rekap-presensi"
  }, {
    path: "/baak/rekap-tugas",
    component: _1bdd373a,
    name: "baak-rekap-tugas"
  }, {
    path: "/baak/support",
    component: _b3b361d2,
    name: "baak-support"
  }, {
    path: "/dosen/beranda",
    component: _235e40c0,
    name: "dosen-beranda"
  }, {
    path: "/dosen/jadwal-online",
    component: _d922491c,
    name: "dosen-jadwal-online"
  }, {
    path: "/dosen/matakuliah",
    component: _5df75598,
    name: "dosen-matakuliah"
  }, {
    path: "/dosen/materi-perkuliahan",
    component: _868a555a,
    name: "dosen-materi-perkuliahan"
  }, {
    path: "/dosen/praktikum",
    component: _33911cee,
    name: "dosen-praktikum"
  }, {
    path: "/dosen/rekap-presensi",
    component: _3faf6c41,
    name: "dosen-rekap-presensi"
  }, {
    path: "/dosen/support",
    component: _58082519,
    name: "dosen-support"
  }, {
    path: "/dosen/tugas-online",
    component: _a704c65e,
    name: "dosen-tugas-online"
  }, {
    path: "/dosen/uas",
    component: _5e4f9a31,
    name: "dosen-uas"
  }, {
    path: "/dosen/uts",
    component: _1f023f84,
    name: "dosen-uts"
  }, {
    path: "/kaprodi/beranda",
    component: _5eb7cd2a,
    name: "kaprodi-beranda"
  }, {
    path: "/kaprodi/rekap-presensi",
    component: _0bce4176,
    name: "kaprodi-rekap-presensi"
  }, {
    path: "/kaprodi/rekap-tugas",
    component: _78c909b2,
    name: "kaprodi-rekap-tugas"
  }, {
    path: "/links/materi",
    component: _7a0fc9bd,
    name: "links-materi"
  }, {
    path: "/links/tugas",
    component: _4f8c2507,
    name: "links-tugas"
  }, {
    path: "/links/video",
    component: _016f3b4a,
    name: "links-video"
  }, {
    path: "/mahasiswa/beranda",
    component: _bafcc06e,
    name: "mahasiswa-beranda"
  }, {
    path: "/mahasiswa/jadwal-online",
    component: _38670fbb,
    name: "mahasiswa-jadwal-online"
  }, {
    path: "/mahasiswa/matakuliah",
    component: _d9b0a422,
    name: "mahasiswa-matakuliah"
  }, {
    path: "/mahasiswa/materi-perkuliahan",
    component: _1402496a,
    name: "mahasiswa-materi-perkuliahan"
  }, {
    path: "/mahasiswa/praktikum",
    component: _29ff3252,
    name: "mahasiswa-praktikum"
  }, {
    path: "/mahasiswa/support",
    component: _05ff4d3c,
    name: "mahasiswa-support"
  }, {
    path: "/mahasiswa/tugas-online",
    component: _56f93b68,
    name: "mahasiswa-tugas-online"
  }, {
    path: "/mahasiswa/uas",
    component: _bbd14a0c,
    name: "mahasiswa-uas"
  }, {
    path: "/mahasiswa/uts",
    component: _3446a107,
    name: "mahasiswa-uts"
  }, {
    path: "/admin/bantuan/generate-token",
    component: _50f9bfde,
    name: "admin-bantuan-generate-token"
  }, {
    path: "/admin/fcm/example",
    component: _d9860a60,
    name: "admin-fcm-example"
  }, {
    path: "/admin/master/mis",
    component: _9ebdc850,
    name: "admin-master-mis"
  }, {
    path: "/admin/master/room-meeting",
    component: _6073207b,
    name: "admin-master-room-meeting"
  }, {
    path: "/admin/master/server-conference",
    component: _4a6d82a4,
    name: "admin-master-server-conference"
  }, {
    path: "/admin/pengaturan/tanggal-libur",
    component: _604dc348,
    name: "admin-pengaturan-tanggal-libur"
  }, {
    path: "/admin/rekap-presensi/detail",
    component: _585f9560,
    name: "admin-rekap-presensi-detail"
  }, {
    path: "/admin/support/detail",
    component: _286ac17c,
    name: "admin-support-detail"
  }, {
    path: "/admin/ujian/uas",
    component: _211c255e,
    name: "admin-ujian-uas"
  }, {
    path: "/admin/ujian/uts",
    component: _4188392b,
    name: "admin-ujian-uts"
  }, {
    path: "/baak/rekap-presensi/detail",
    component: _9f471120,
    name: "baak-rekap-presensi-detail"
  }, {
    path: "/baak/rekap-tugas/detail",
    component: _0fc680ce,
    name: "baak-rekap-tugas-detail"
  }, {
    path: "/baak/support/detail",
    component: _9212b948,
    name: "baak-support-detail"
  }, {
    path: "/baak/ujian/uas",
    component: _0d62977e,
    name: "baak-ujian-uas"
  }, {
    path: "/baak/ujian/uts",
    component: _2dceab4b,
    name: "baak-ujian-uts"
  }, {
    path: "/dosen/kuliah/detail",
    component: _4545458e,
    name: "dosen-kuliah-detail"
  }, {
    path: "/dosen/kuliah/forum",
    component: _4ba943a8,
    name: "dosen-kuliah-forum"
  }, {
    path: "/dosen/kuliah/mahasiswa",
    component: _dacb787c,
    name: "dosen-kuliah-mahasiswa"
  }, {
    path: "/dosen/kuliah/materi",
    component: _05aadf64,
    name: "dosen-kuliah-materi"
  }, {
    path: "/dosen/kuliah/pengumuman",
    component: _70b0e19a,
    name: "dosen-kuliah-pengumuman"
  }, {
    path: "/dosen/kuliah/tugas",
    component: _1e178300,
    name: "dosen-kuliah-tugas"
  }, {
    path: "/dosen/kuliah/video",
    component: _ba51567a,
    name: "dosen-kuliah-video"
  }, {
    path: "/dosen/materi-perkuliahan/daftar-materi",
    component: _3b5c796e,
    name: "dosen-materi-perkuliahan-daftar-materi"
  }, {
    path: "/dosen/rekap-presensi/detail",
    component: _4e912b72,
    name: "dosen-rekap-presensi-detail"
  }, {
    path: "/dosen/support/buat",
    component: _577e1acf,
    name: "dosen-support-buat"
  }, {
    path: "/dosen/support/detail",
    component: _414f8d9a,
    name: "dosen-support-detail"
  }, {
    path: "/dosen/uas/detail",
    component: _03f6bb82,
    name: "dosen-uas-detail"
  }, {
    path: "/dosen/uts/detail",
    component: _909f91d6,
    name: "dosen-uts-detail"
  }, {
    path: "/kaprodi/rekap-presensi/detail",
    component: _064cfcdd,
    name: "kaprodi-rekap-presensi-detail"
  }, {
    path: "/kaprodi/rekap-tugas/detail",
    component: _4826f94c,
    name: "kaprodi-rekap-tugas-detail"
  }, {
    path: "/mahasiswa/kuliah/detail",
    component: _fb54dcfc,
    name: "mahasiswa-kuliah-detail"
  }, {
    path: "/mahasiswa/kuliah/forum",
    component: _6e7520fa,
    name: "mahasiswa-kuliah-forum"
  }, {
    path: "/mahasiswa/kuliah/mahasiswa",
    component: _4266bed9,
    name: "mahasiswa-kuliah-mahasiswa"
  }, {
    path: "/mahasiswa/kuliah/materi",
    component: _aab9d8a6,
    name: "mahasiswa-kuliah-materi"
  }, {
    path: "/mahasiswa/kuliah/pengumuman",
    component: _116a76fc,
    name: "mahasiswa-kuliah-pengumuman"
  }, {
    path: "/mahasiswa/kuliah/tugas",
    component: _c92045d2,
    name: "mahasiswa-kuliah-tugas"
  }, {
    path: "/mahasiswa/kuliah/video",
    component: _4d52f35a,
    name: "mahasiswa-kuliah-video"
  }, {
    path: "/mahasiswa/materi-perkuliahan/daftar-materi",
    component: _9fd432f6,
    name: "mahasiswa-materi-perkuliahan-daftar-materi"
  }, {
    path: "/mahasiswa/support/buat",
    component: _01f9b966,
    name: "mahasiswa-support-buat"
  }, {
    path: "/mahasiswa/support/detail",
    component: _3b5de271,
    name: "mahasiswa-support-detail"
  }, {
    path: "/mahasiswa/uas/detail",
    component: _392713d9,
    name: "mahasiswa-uas-detail"
  }, {
    path: "/mahasiswa/uas/detail-ujian",
    component: _2503e6a6,
    name: "mahasiswa-uas-detail-ujian"
  }, {
    path: "/mahasiswa/uts/detail",
    component: _263ee128,
    name: "mahasiswa-uts-detail"
  }, {
    path: "/mahasiswa/uts/detail-ujian",
    component: _31234e00,
    name: "mahasiswa-uts-detail-ujian"
  }, {
    path: "/admin/master/mis/agama",
    component: _b5f53f56,
    name: "admin-master-mis-agama"
  }, {
    path: "/admin/master/mis/hari",
    component: _6764c74c,
    name: "admin-master-mis-hari"
  }, {
    path: "/admin/master/mis/jam",
    component: _25b0ce9c,
    name: "admin-master-mis-jam"
  }, {
    path: "/admin/master/mis/jam-pjj",
    component: _7581697f,
    name: "admin-master-mis-jam-pjj"
  }, {
    path: "/admin/master/mis/jam-psdku",
    component: _2788770c,
    name: "admin-master-mis-jam-psdku"
  }, {
    path: "/admin/master/mis/jam-reguler",
    component: _31b61f32,
    name: "admin-master-mis-jam-reguler"
  }, {
    path: "/admin/master/mis/jurusan",
    component: _37787bd8,
    name: "admin-master-mis-jurusan"
  }, {
    path: "/admin/master/mis/kelas",
    component: _5b512fec,
    name: "admin-master-mis-kelas"
  }, {
    path: "/admin/master/mis/kuliah",
    component: _4867c6b8,
    name: "admin-master-mis-kuliah"
  }, {
    path: "/admin/master/mis/kuliah-agama",
    component: _5613c57a,
    name: "admin-master-mis-kuliah-agama"
  }, {
    path: "/admin/master/mis/kuliah-agama-mahasiswa",
    component: _470ae9e7,
    name: "admin-master-mis-kuliah-agama-mahasiswa"
  }, {
    path: "/admin/master/mis/kuliah-pararel",
    component: _0b4a06b8,
    name: "admin-master-mis-kuliah-pararel"
  }, {
    path: "/admin/master/mis/mahasiswa",
    component: _4917ab00,
    name: "admin-master-mis-mahasiswa"
  }, {
    path: "/admin/master/mis/mahasiswa-semester",
    component: _31f18915,
    name: "admin-master-mis-mahasiswa-semester"
  }, {
    path: "/admin/master/mis/matakuliah",
    component: _6327710e,
    name: "admin-master-mis-matakuliah"
  }, {
    path: "/admin/master/mis/pegawai",
    component: _71f06914,
    name: "admin-master-mis-pegawai"
  }, {
    path: "/admin/master/mis/program",
    component: _37e6450a,
    name: "admin-master-mis-program"
  }, {
    path: "/admin/master/mis/ruang-kuliah",
    component: _a7a177e4,
    name: "admin-master-mis-ruang-kuliah"
  }, {
    path: "/admin/master/mis/skema",
    component: _f61a6232,
    name: "admin-master-mis-skema"
  }, {
    path: "/admin/master/mis/soal",
    component: _0ab9ab71,
    name: "admin-master-mis-soal"
  }, {
    path: "/admin/master/mis/soal-agama",
    component: _c269091a,
    name: "admin-master-mis-soal-agama"
  }, {
    path: "/dosen/kuliah/rekap-nilai/tugas",
    component: _f97d4238,
    name: "dosen-kuliah-rekap-nilai-tugas"
  }, {
    path: "/dosen/kuliah/rekap-nilai/uas",
    component: _2198a313,
    name: "dosen-kuliah-rekap-nilai-uas"
  }, {
    path: "/dosen/kuliah/rekap-nilai/uts",
    component: _4204b6e0,
    name: "dosen-kuliah-rekap-nilai-uts"
  }, {
    path: "/dosen/notifikasi/presensi/:id?",
    component: _ed1cbb6a,
    name: "dosen-notifikasi-presensi-id"
  }, {
    path: "/dosen/notifikasi/tugas/:id?",
    component: _389fdcfc,
    name: "dosen-notifikasi-tugas-id"
  }, {
    path: "/mahasiswa/notifikasi/materi/:id?",
    component: _0df2c83e,
    name: "mahasiswa-notifikasi-materi-id"
  }, {
    path: "/mahasiswa/notifikasi/presensi/:id?",
    component: _41efcf54,
    name: "mahasiswa-notifikasi-presensi-id"
  }, {
    path: "/mahasiswa/notifikasi/tugas/:id?",
    component: _d7e9adce,
    name: "mahasiswa-notifikasi-tugas-id"
  }, {
    path: "/mahasiswa/notifikasi/video/:id?",
    component: _53b93448,
    name: "mahasiswa-notifikasi-video-id"
  }, {
    path: "/",
    component: _d3350e92,
    name: "index"
  }],

  fallback: false
}

export function createRouter (ssrContext, config) {
  const base = (config.app && config.app.basePath) || routerOptions.base
  const router = new Router({ ...routerOptions, base  })

  // TODO: remove in Nuxt 3
  const originalPush = router.push
  router.push = function push (location, onComplete = emptyFn, onAbort) {
    return originalPush.call(this, location, onComplete, onAbort)
  }

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    return resolve(to, current, append)
  }

  return router
}
