export { default as LineChart } from '../../components/LineChart.vue'
export { default as PieChart } from '../../components/PieChart.vue'
export { default as VideoPlayer } from '../../components/Video/Player.vue'
export { default as BackendBreadcrumb } from '../../components/backend/breadcrumb.vue'
export { default as BerandaCardMateri } from '../../components/beranda/card-materi.vue'
export { default as BerandaCardSelamatDatang } from '../../components/beranda/card-selamat-datang.vue'
export { default as BerandaDaftarMateri } from '../../components/beranda/daftar-materi.vue'
export { default as BerandaDialogKelasVirtual } from '../../components/beranda/dialog-kelas-virtual.vue'
export { default as BerandaEtholMenu } from '../../components/beranda/ethol-menu.vue'
export { default as BerandaMateri } from '../../components/beranda/materi.vue'
export { default as BerandaPraktikum } from '../../components/beranda/praktikum.vue'
export { default as CardFile } from '../../components/card/card-file.vue'
export { default as CardWithLeftIcon } from '../../components/card/card-with-left-icon.vue'
export { default as FilterBulanIndonesia } from '../../components/filter/bulan-indonesia.vue'
export { default as FilterJurusanKaprodi } from '../../components/filter/jurusan-kaprodi.vue'
export { default as FilterJurusan } from '../../components/filter/jurusan.vue'
export { default as FilterProgramKaprodi } from '../../components/filter/program-kaprodi.vue'
export { default as FilterProgram } from '../../components/filter/program.vue'
export { default as FilterSemester } from '../../components/filter/semester.vue'
export { default as FilterTahunAjaran } from '../../components/filter/tahun-ajaran.vue'
export { default as FilterTahun } from '../../components/filter/tahun.vue'
export { default as ForumCardBuatForum } from '../../components/forum/card-buat-forum.vue'
export { default as ForumCardPost } from '../../components/forum/card-post.vue'
export { default as ForumContentLoading } from '../../components/forum/content-loading.vue'
export { default as ForumDialogBuatPost } from '../../components/forum/dialog-buat-post.vue'
export { default as ForumDialogEditPost } from '../../components/forum/dialog-edit-post.vue'
export { default as FrontendCardChooseUs } from '../../components/frontend/card-choose-us.vue'
export { default as FrontendCardServices } from '../../components/frontend/card-services.vue'
export { default as FrontendCarousel } from '../../components/frontend/carousel.vue'
export { default as FrontendDialogLogin } from '../../components/frontend/dialog-login.vue'
export { default as FrontendFaq } from '../../components/frontend/faq.vue'
export { default as InfoCardInfo } from '../../components/info/card-info.vue'
export { default as JadwalKuliahDialogGenerateJadwal } from '../../components/jadwal-kuliah/dialog-generate-jadwal.vue'
export { default as JadwalKuliah } from '../../components/jadwal-kuliah/jadwal-kuliah.vue'
export { default as KuliahCardInfoMahasiswa } from '../../components/kuliah/card-info-mahasiswa.vue'
export { default as KuliahCardKuliahDosen } from '../../components/kuliah/card-kuliah-dosen.vue'
export { default as KuliahCardKuliahMahasiwa } from '../../components/kuliah/card-kuliah-mahasiwa.vue'
export { default as KuliahCardMahasiswa } from '../../components/kuliah/card-mahasiswa.vue'
export { default as KuliahDialogFormConferenceLainnya } from '../../components/kuliah/dialog-form-conference-lainnya.vue'
export { default as KuliahDialogRulePresensiKuliah } from '../../components/kuliah/dialog-rule-presensi-kuliah.vue'
export { default as KuliahSlideKuliah } from '../../components/kuliah/slide-kuliah.vue'
export { default as KuliahSlideLoadingKuliah } from '../../components/kuliah/slide-loading-kuliah.vue'
export { default as MenuGroup } from '../../components/menu/group.vue'
export { default as MenuLogout } from '../../components/menu/logout.vue'
export { default as MenuSingle } from '../../components/menu/single.vue'
export { default as NotifikasiCardNotifikasi } from '../../components/notifikasi/card-notifikasi.vue'
export { default as NotifikasiDetailNotifikasiMateri } from '../../components/notifikasi/detail-notifikasi-materi.vue'
export { default as NotifikasiDetailNotifikasiPresensi } from '../../components/notifikasi/detail-notifikasi-presensi.vue'
export { default as NotifikasiDetailNotifikasiTugas } from '../../components/notifikasi/detail-notifikasi-tugas.vue'
export { default as NotifikasiDetailNotifikasiVideo } from '../../components/notifikasi/detail-notifikasi-video.vue'
export { default as NotifikasiHeaderNotifikasi } from '../../components/notifikasi/header-notifikasi.vue'
export { default as NotifikasiPopupDrawerNotifikasi } from '../../components/notifikasi/popup-drawer-notifikasi.vue'
export { default as PengumumanDialog } from '../../components/pengumuman/dialog.vue'
export { default as PresensiDetailPresensiHeaderDosen } from '../../components/presensi/detail-presensi-header-dosen.vue'
export { default as PresensiDetailPresensiHeaderKaprodi } from '../../components/presensi/detail-presensi-header-kaprodi.vue'
export { default as PresensiDetailPresensiHeader } from '../../components/presensi/detail-presensi-header.vue'
export { default as PresensiDetailPresensiKaprodi } from '../../components/presensi/detail-presensi-kaprodi.vue'
export { default as PresensiDetailPresensi } from '../../components/presensi/detail-presensi.vue'
export { default as PresensiDialogInputPresensi } from '../../components/presensi/dialog-input-presensi.vue'
export { default as PresensiRekapPresensiKaprodi } from '../../components/presensi/rekap-presensi-kaprodi.vue'
export { default as PresensiRekapPresensi } from '../../components/presensi/rekap-presensi.vue'
export { default as SupportBuatSupport } from '../../components/support/buatSupport.vue'
export { default as SupportDetailSupport } from '../../components/support/detailSupport.vue'
export { default as SupportDialogTunjuk } from '../../components/support/dialogTunjuk.vue'
export { default as SupportAdmin } from '../../components/support/support-admin.vue'
export { default as SupportBaak } from '../../components/support/support-baak.vue'
export { default as Support } from '../../components/support/support.vue'
export { default as SurveiDialogSurveiPenilaian } from '../../components/survei/dialog-survei-penilaian.vue'
export { default as TugasDetailTugasHeaderKaprodi } from '../../components/tugas/detail-tugas-header-kaprodi.vue'
export { default as TugasDetailTugasHeader } from '../../components/tugas/detail-tugas-header.vue'
export { default as TugasDetailTugasKaprodi } from '../../components/tugas/detail-tugas-kaprodi.vue'
export { default as TugasDetailTugas } from '../../components/tugas/detail-tugas.vue'
export { default as TugasRekapTugasKaprodi } from '../../components/tugas/rekap-tugas-kaprodi.vue'
export { default as TugasRekapTugas } from '../../components/tugas/rekap-tugas.vue'
export { default as UjianCardBodyDetailUjianMhs } from '../../components/ujian/card-body-detail-ujian-mhs.vue'
export { default as UjianCardHeaderDetailUjianAgama } from '../../components/ujian/card-header-detail-ujian-agama.vue'
export { default as UjianCardHeaderDetailUjian } from '../../components/ujian/card-header-detail-ujian.vue'
export { default as UjianCardUjianDosen } from '../../components/ujian/card-ujian-dosen.vue'
export { default as UjianCardUjianMahasiswa } from '../../components/ujian/card-ujian-mahasiswa.vue'
export { default as UjianDialogBatasPengumpulanDosen } from '../../components/ujian/dialog-batas-pengumpulan-dosen.vue'
export { default as UjianDialogEditDosen } from '../../components/ujian/dialog-edit-dosen.vue'
export { default as UjianDialogEditJadwal } from '../../components/ujian/dialog-edit-jadwal.vue'
export { default as UjianDialogEditTanggal } from '../../components/ujian/dialog-edit-tanggal.vue'
export { default as UjianDialogPilihUjian } from '../../components/ujian/dialog-pilih-ujian.vue'
export { default as UjianDialogSubmitNilaiUjian } from '../../components/ujian/dialog-submit-nilai-ujian.vue'
export { default as UjianDialogTambahJadwal } from '../../components/ujian/dialog-tambah-jadwal.vue'
export { default as UjianDialogTarikSoal } from '../../components/ujian/dialog-tarik-soal.vue'
export { default as UjianListJadwal } from '../../components/ujian/list-jadwal.vue'
export { default as UserProfil } from '../../components/user/profil.vue'
export { default as UtilsDateTimePicker } from '../../components/utils/date-time-picker.vue'
export { default as UtilsDialogDelete } from '../../components/utils/dialog-delete.vue'
export { default as UtilsDialogKonfirmasi } from '../../components/utils/dialog-konfirmasi.vue'
export { default as UtilsSnackbar } from '../../components/utils/snackbar.vue'
export { default as WysiwygEditor } from '../../components/wysiwyg/editor.vue'
export { default as FilterMisAngkatan } from '../../components/filter/mis/angkatan.vue'
export { default as FilterMisHari } from '../../components/filter/mis/hari.vue'
export { default as FilterMisJenisSchema } from '../../components/filter/mis/jenis-schema.vue'
export { default as FilterMisJurusan } from '../../components/filter/mis/jurusan.vue'
export { default as FilterMisProgram } from '../../components/filter/mis/program.vue'
export { default as KuliahMateriCardMateri } from '../../components/kuliah/materi/card-materi.vue'
export { default as KuliahMateriDialogLihatMateri } from '../../components/kuliah/materi/dialog-lihat-materi.vue'
export { default as KuliahMateriDialogUploadMateriBaru } from '../../components/kuliah/materi/dialog-upload-materi-baru.vue'
export { default as KuliahMateriDialogUploadMateriSebelumnya } from '../../components/kuliah/materi/dialog-upload-materi-sebelumnya.vue'
export { default as KuliahPengumumanCardPengumuman } from '../../components/kuliah/pengumuman/card-pengumuman.vue'
export { default as KuliahPengumumanCardSemuaPengumuman } from '../../components/kuliah/pengumuman/card-semua-pengumuman.vue'
export { default as KuliahPengumumanDialog } from '../../components/kuliah/pengumuman/dialog.vue'
export { default as KuliahTugasCardTugas } from '../../components/kuliah/tugas/card-tugas.vue'
export { default as KuliahTugasDialogDetailTugasDosen } from '../../components/kuliah/tugas/dialog-detail-tugas-dosen.vue'
export { default as KuliahTugasDialogDetailTugasMahasiswa } from '../../components/kuliah/tugas/dialog-detail-tugas-mahasiswa.vue'
export { default as KuliahTugasDialogFormTugas } from '../../components/kuliah/tugas/dialog-form-tugas.vue'
export { default as KuliahTugasDialogSubmitNilai } from '../../components/kuliah/tugas/dialog-submit-nilai.vue'
export { default as KuliahTugasDialogSubmitTugas } from '../../components/kuliah/tugas/dialog-submit-tugas.vue'
export { default as KuliahVideoCardVideo } from '../../components/kuliah/video/card-video.vue'
export { default as KuliahVideoDialogLihatVideo } from '../../components/kuliah/video/dialog-lihat-video.vue'
export { default as KuliahVideoDialog } from '../../components/kuliah/video/dialog.vue'
export { default as MasterRoomConferenceDialog } from '../../components/master/room-conference/dialog.vue'
export { default as MasterServerConferenceDialog } from '../../components/master/server-conference/dialog.vue'
export { default as MenuHakAdmin } from '../../components/menu/hak/admin.vue'
export { default as MenuHakBaak } from '../../components/menu/hak/baak.vue'
export { default as MenuHakDosen } from '../../components/menu/hak/dosen.vue'
export { default as MenuHakKaprodi } from '../../components/menu/hak/kaprodi.vue'
export { default as MenuHakMahasiswa } from '../../components/menu/hak/mahasiswa.vue'
export { default as PengaturanTanggalLiburDialog } from '../../components/pengaturan/tanggal-libur/dialog.vue'

export const LazyLineChart = import('../../components/LineChart.vue' /* webpackChunkName: "components/line-chart" */).then(c => c.default || c)
export const LazyPieChart = import('../../components/PieChart.vue' /* webpackChunkName: "components/pie-chart" */).then(c => c.default || c)
export const LazyVideoPlayer = import('../../components/Video/Player.vue' /* webpackChunkName: "components/video-player" */).then(c => c.default || c)
export const LazyBackendBreadcrumb = import('../../components/backend/breadcrumb.vue' /* webpackChunkName: "components/backend-breadcrumb" */).then(c => c.default || c)
export const LazyBerandaCardMateri = import('../../components/beranda/card-materi.vue' /* webpackChunkName: "components/beranda-card-materi" */).then(c => c.default || c)
export const LazyBerandaCardSelamatDatang = import('../../components/beranda/card-selamat-datang.vue' /* webpackChunkName: "components/beranda-card-selamat-datang" */).then(c => c.default || c)
export const LazyBerandaDaftarMateri = import('../../components/beranda/daftar-materi.vue' /* webpackChunkName: "components/beranda-daftar-materi" */).then(c => c.default || c)
export const LazyBerandaDialogKelasVirtual = import('../../components/beranda/dialog-kelas-virtual.vue' /* webpackChunkName: "components/beranda-dialog-kelas-virtual" */).then(c => c.default || c)
export const LazyBerandaEtholMenu = import('../../components/beranda/ethol-menu.vue' /* webpackChunkName: "components/beranda-ethol-menu" */).then(c => c.default || c)
export const LazyBerandaMateri = import('../../components/beranda/materi.vue' /* webpackChunkName: "components/beranda-materi" */).then(c => c.default || c)
export const LazyBerandaPraktikum = import('../../components/beranda/praktikum.vue' /* webpackChunkName: "components/beranda-praktikum" */).then(c => c.default || c)
export const LazyCardFile = import('../../components/card/card-file.vue' /* webpackChunkName: "components/card-file" */).then(c => c.default || c)
export const LazyCardWithLeftIcon = import('../../components/card/card-with-left-icon.vue' /* webpackChunkName: "components/card-with-left-icon" */).then(c => c.default || c)
export const LazyFilterBulanIndonesia = import('../../components/filter/bulan-indonesia.vue' /* webpackChunkName: "components/filter-bulan-indonesia" */).then(c => c.default || c)
export const LazyFilterJurusanKaprodi = import('../../components/filter/jurusan-kaprodi.vue' /* webpackChunkName: "components/filter-jurusan-kaprodi" */).then(c => c.default || c)
export const LazyFilterJurusan = import('../../components/filter/jurusan.vue' /* webpackChunkName: "components/filter-jurusan" */).then(c => c.default || c)
export const LazyFilterProgramKaprodi = import('../../components/filter/program-kaprodi.vue' /* webpackChunkName: "components/filter-program-kaprodi" */).then(c => c.default || c)
export const LazyFilterProgram = import('../../components/filter/program.vue' /* webpackChunkName: "components/filter-program" */).then(c => c.default || c)
export const LazyFilterSemester = import('../../components/filter/semester.vue' /* webpackChunkName: "components/filter-semester" */).then(c => c.default || c)
export const LazyFilterTahunAjaran = import('../../components/filter/tahun-ajaran.vue' /* webpackChunkName: "components/filter-tahun-ajaran" */).then(c => c.default || c)
export const LazyFilterTahun = import('../../components/filter/tahun.vue' /* webpackChunkName: "components/filter-tahun" */).then(c => c.default || c)
export const LazyForumCardBuatForum = import('../../components/forum/card-buat-forum.vue' /* webpackChunkName: "components/forum-card-buat-forum" */).then(c => c.default || c)
export const LazyForumCardPost = import('../../components/forum/card-post.vue' /* webpackChunkName: "components/forum-card-post" */).then(c => c.default || c)
export const LazyForumContentLoading = import('../../components/forum/content-loading.vue' /* webpackChunkName: "components/forum-content-loading" */).then(c => c.default || c)
export const LazyForumDialogBuatPost = import('../../components/forum/dialog-buat-post.vue' /* webpackChunkName: "components/forum-dialog-buat-post" */).then(c => c.default || c)
export const LazyForumDialogEditPost = import('../../components/forum/dialog-edit-post.vue' /* webpackChunkName: "components/forum-dialog-edit-post" */).then(c => c.default || c)
export const LazyFrontendCardChooseUs = import('../../components/frontend/card-choose-us.vue' /* webpackChunkName: "components/frontend-card-choose-us" */).then(c => c.default || c)
export const LazyFrontendCardServices = import('../../components/frontend/card-services.vue' /* webpackChunkName: "components/frontend-card-services" */).then(c => c.default || c)
export const LazyFrontendCarousel = import('../../components/frontend/carousel.vue' /* webpackChunkName: "components/frontend-carousel" */).then(c => c.default || c)
export const LazyFrontendDialogLogin = import('../../components/frontend/dialog-login.vue' /* webpackChunkName: "components/frontend-dialog-login" */).then(c => c.default || c)
export const LazyFrontendFaq = import('../../components/frontend/faq.vue' /* webpackChunkName: "components/frontend-faq" */).then(c => c.default || c)
export const LazyInfoCardInfo = import('../../components/info/card-info.vue' /* webpackChunkName: "components/info-card-info" */).then(c => c.default || c)
export const LazyJadwalKuliahDialogGenerateJadwal = import('../../components/jadwal-kuliah/dialog-generate-jadwal.vue' /* webpackChunkName: "components/jadwal-kuliah-dialog-generate-jadwal" */).then(c => c.default || c)
export const LazyJadwalKuliah = import('../../components/jadwal-kuliah/jadwal-kuliah.vue' /* webpackChunkName: "components/jadwal-kuliah" */).then(c => c.default || c)
export const LazyKuliahCardInfoMahasiswa = import('../../components/kuliah/card-info-mahasiswa.vue' /* webpackChunkName: "components/kuliah-card-info-mahasiswa" */).then(c => c.default || c)
export const LazyKuliahCardKuliahDosen = import('../../components/kuliah/card-kuliah-dosen.vue' /* webpackChunkName: "components/kuliah-card-kuliah-dosen" */).then(c => c.default || c)
export const LazyKuliahCardKuliahMahasiwa = import('../../components/kuliah/card-kuliah-mahasiwa.vue' /* webpackChunkName: "components/kuliah-card-kuliah-mahasiwa" */).then(c => c.default || c)
export const LazyKuliahCardMahasiswa = import('../../components/kuliah/card-mahasiswa.vue' /* webpackChunkName: "components/kuliah-card-mahasiswa" */).then(c => c.default || c)
export const LazyKuliahDialogFormConferenceLainnya = import('../../components/kuliah/dialog-form-conference-lainnya.vue' /* webpackChunkName: "components/kuliah-dialog-form-conference-lainnya" */).then(c => c.default || c)
export const LazyKuliahDialogRulePresensiKuliah = import('../../components/kuliah/dialog-rule-presensi-kuliah.vue' /* webpackChunkName: "components/kuliah-dialog-rule-presensi-kuliah" */).then(c => c.default || c)
export const LazyKuliahSlideKuliah = import('../../components/kuliah/slide-kuliah.vue' /* webpackChunkName: "components/kuliah-slide-kuliah" */).then(c => c.default || c)
export const LazyKuliahSlideLoadingKuliah = import('../../components/kuliah/slide-loading-kuliah.vue' /* webpackChunkName: "components/kuliah-slide-loading-kuliah" */).then(c => c.default || c)
export const LazyMenuGroup = import('../../components/menu/group.vue' /* webpackChunkName: "components/menu-group" */).then(c => c.default || c)
export const LazyMenuLogout = import('../../components/menu/logout.vue' /* webpackChunkName: "components/menu-logout" */).then(c => c.default || c)
export const LazyMenuSingle = import('../../components/menu/single.vue' /* webpackChunkName: "components/menu-single" */).then(c => c.default || c)
export const LazyNotifikasiCardNotifikasi = import('../../components/notifikasi/card-notifikasi.vue' /* webpackChunkName: "components/notifikasi-card-notifikasi" */).then(c => c.default || c)
export const LazyNotifikasiDetailNotifikasiMateri = import('../../components/notifikasi/detail-notifikasi-materi.vue' /* webpackChunkName: "components/notifikasi-detail-notifikasi-materi" */).then(c => c.default || c)
export const LazyNotifikasiDetailNotifikasiPresensi = import('../../components/notifikasi/detail-notifikasi-presensi.vue' /* webpackChunkName: "components/notifikasi-detail-notifikasi-presensi" */).then(c => c.default || c)
export const LazyNotifikasiDetailNotifikasiTugas = import('../../components/notifikasi/detail-notifikasi-tugas.vue' /* webpackChunkName: "components/notifikasi-detail-notifikasi-tugas" */).then(c => c.default || c)
export const LazyNotifikasiDetailNotifikasiVideo = import('../../components/notifikasi/detail-notifikasi-video.vue' /* webpackChunkName: "components/notifikasi-detail-notifikasi-video" */).then(c => c.default || c)
export const LazyNotifikasiHeaderNotifikasi = import('../../components/notifikasi/header-notifikasi.vue' /* webpackChunkName: "components/notifikasi-header-notifikasi" */).then(c => c.default || c)
export const LazyNotifikasiPopupDrawerNotifikasi = import('../../components/notifikasi/popup-drawer-notifikasi.vue' /* webpackChunkName: "components/notifikasi-popup-drawer-notifikasi" */).then(c => c.default || c)
export const LazyPengumumanDialog = import('../../components/pengumuman/dialog.vue' /* webpackChunkName: "components/pengumuman-dialog" */).then(c => c.default || c)
export const LazyPresensiDetailPresensiHeaderDosen = import('../../components/presensi/detail-presensi-header-dosen.vue' /* webpackChunkName: "components/presensi-detail-presensi-header-dosen" */).then(c => c.default || c)
export const LazyPresensiDetailPresensiHeaderKaprodi = import('../../components/presensi/detail-presensi-header-kaprodi.vue' /* webpackChunkName: "components/presensi-detail-presensi-header-kaprodi" */).then(c => c.default || c)
export const LazyPresensiDetailPresensiHeader = import('../../components/presensi/detail-presensi-header.vue' /* webpackChunkName: "components/presensi-detail-presensi-header" */).then(c => c.default || c)
export const LazyPresensiDetailPresensiKaprodi = import('../../components/presensi/detail-presensi-kaprodi.vue' /* webpackChunkName: "components/presensi-detail-presensi-kaprodi" */).then(c => c.default || c)
export const LazyPresensiDetailPresensi = import('../../components/presensi/detail-presensi.vue' /* webpackChunkName: "components/presensi-detail-presensi" */).then(c => c.default || c)
export const LazyPresensiDialogInputPresensi = import('../../components/presensi/dialog-input-presensi.vue' /* webpackChunkName: "components/presensi-dialog-input-presensi" */).then(c => c.default || c)
export const LazyPresensiRekapPresensiKaprodi = import('../../components/presensi/rekap-presensi-kaprodi.vue' /* webpackChunkName: "components/presensi-rekap-presensi-kaprodi" */).then(c => c.default || c)
export const LazyPresensiRekapPresensi = import('../../components/presensi/rekap-presensi.vue' /* webpackChunkName: "components/presensi-rekap-presensi" */).then(c => c.default || c)
export const LazySupportBuatSupport = import('../../components/support/buatSupport.vue' /* webpackChunkName: "components/support-buat-support" */).then(c => c.default || c)
export const LazySupportDetailSupport = import('../../components/support/detailSupport.vue' /* webpackChunkName: "components/support-detail-support" */).then(c => c.default || c)
export const LazySupportDialogTunjuk = import('../../components/support/dialogTunjuk.vue' /* webpackChunkName: "components/support-dialog-tunjuk" */).then(c => c.default || c)
export const LazySupportAdmin = import('../../components/support/support-admin.vue' /* webpackChunkName: "components/support-admin" */).then(c => c.default || c)
export const LazySupportBaak = import('../../components/support/support-baak.vue' /* webpackChunkName: "components/support-baak" */).then(c => c.default || c)
export const LazySupport = import('../../components/support/support.vue' /* webpackChunkName: "components/support" */).then(c => c.default || c)
export const LazySurveiDialogSurveiPenilaian = import('../../components/survei/dialog-survei-penilaian.vue' /* webpackChunkName: "components/survei-dialog-survei-penilaian" */).then(c => c.default || c)
export const LazyTugasDetailTugasHeaderKaprodi = import('../../components/tugas/detail-tugas-header-kaprodi.vue' /* webpackChunkName: "components/tugas-detail-tugas-header-kaprodi" */).then(c => c.default || c)
export const LazyTugasDetailTugasHeader = import('../../components/tugas/detail-tugas-header.vue' /* webpackChunkName: "components/tugas-detail-tugas-header" */).then(c => c.default || c)
export const LazyTugasDetailTugasKaprodi = import('../../components/tugas/detail-tugas-kaprodi.vue' /* webpackChunkName: "components/tugas-detail-tugas-kaprodi" */).then(c => c.default || c)
export const LazyTugasDetailTugas = import('../../components/tugas/detail-tugas.vue' /* webpackChunkName: "components/tugas-detail-tugas" */).then(c => c.default || c)
export const LazyTugasRekapTugasKaprodi = import('../../components/tugas/rekap-tugas-kaprodi.vue' /* webpackChunkName: "components/tugas-rekap-tugas-kaprodi" */).then(c => c.default || c)
export const LazyTugasRekapTugas = import('../../components/tugas/rekap-tugas.vue' /* webpackChunkName: "components/tugas-rekap-tugas" */).then(c => c.default || c)
export const LazyUjianCardBodyDetailUjianMhs = import('../../components/ujian/card-body-detail-ujian-mhs.vue' /* webpackChunkName: "components/ujian-card-body-detail-ujian-mhs" */).then(c => c.default || c)
export const LazyUjianCardHeaderDetailUjianAgama = import('../../components/ujian/card-header-detail-ujian-agama.vue' /* webpackChunkName: "components/ujian-card-header-detail-ujian-agama" */).then(c => c.default || c)
export const LazyUjianCardHeaderDetailUjian = import('../../components/ujian/card-header-detail-ujian.vue' /* webpackChunkName: "components/ujian-card-header-detail-ujian" */).then(c => c.default || c)
export const LazyUjianCardUjianDosen = import('../../components/ujian/card-ujian-dosen.vue' /* webpackChunkName: "components/ujian-card-ujian-dosen" */).then(c => c.default || c)
export const LazyUjianCardUjianMahasiswa = import('../../components/ujian/card-ujian-mahasiswa.vue' /* webpackChunkName: "components/ujian-card-ujian-mahasiswa" */).then(c => c.default || c)
export const LazyUjianDialogBatasPengumpulanDosen = import('../../components/ujian/dialog-batas-pengumpulan-dosen.vue' /* webpackChunkName: "components/ujian-dialog-batas-pengumpulan-dosen" */).then(c => c.default || c)
export const LazyUjianDialogEditDosen = import('../../components/ujian/dialog-edit-dosen.vue' /* webpackChunkName: "components/ujian-dialog-edit-dosen" */).then(c => c.default || c)
export const LazyUjianDialogEditJadwal = import('../../components/ujian/dialog-edit-jadwal.vue' /* webpackChunkName: "components/ujian-dialog-edit-jadwal" */).then(c => c.default || c)
export const LazyUjianDialogEditTanggal = import('../../components/ujian/dialog-edit-tanggal.vue' /* webpackChunkName: "components/ujian-dialog-edit-tanggal" */).then(c => c.default || c)
export const LazyUjianDialogPilihUjian = import('../../components/ujian/dialog-pilih-ujian.vue' /* webpackChunkName: "components/ujian-dialog-pilih-ujian" */).then(c => c.default || c)
export const LazyUjianDialogSubmitNilaiUjian = import('../../components/ujian/dialog-submit-nilai-ujian.vue' /* webpackChunkName: "components/ujian-dialog-submit-nilai-ujian" */).then(c => c.default || c)
export const LazyUjianDialogTambahJadwal = import('../../components/ujian/dialog-tambah-jadwal.vue' /* webpackChunkName: "components/ujian-dialog-tambah-jadwal" */).then(c => c.default || c)
export const LazyUjianDialogTarikSoal = import('../../components/ujian/dialog-tarik-soal.vue' /* webpackChunkName: "components/ujian-dialog-tarik-soal" */).then(c => c.default || c)
export const LazyUjianListJadwal = import('../../components/ujian/list-jadwal.vue' /* webpackChunkName: "components/ujian-list-jadwal" */).then(c => c.default || c)
export const LazyUserProfil = import('../../components/user/profil.vue' /* webpackChunkName: "components/user-profil" */).then(c => c.default || c)
export const LazyUtilsDateTimePicker = import('../../components/utils/date-time-picker.vue' /* webpackChunkName: "components/utils-date-time-picker" */).then(c => c.default || c)
export const LazyUtilsDialogDelete = import('../../components/utils/dialog-delete.vue' /* webpackChunkName: "components/utils-dialog-delete" */).then(c => c.default || c)
export const LazyUtilsDialogKonfirmasi = import('../../components/utils/dialog-konfirmasi.vue' /* webpackChunkName: "components/utils-dialog-konfirmasi" */).then(c => c.default || c)
export const LazyUtilsSnackbar = import('../../components/utils/snackbar.vue' /* webpackChunkName: "components/utils-snackbar" */).then(c => c.default || c)
export const LazyWysiwygEditor = import('../../components/wysiwyg/editor.vue' /* webpackChunkName: "components/wysiwyg-editor" */).then(c => c.default || c)
export const LazyFilterMisAngkatan = import('../../components/filter/mis/angkatan.vue' /* webpackChunkName: "components/filter-mis-angkatan" */).then(c => c.default || c)
export const LazyFilterMisHari = import('../../components/filter/mis/hari.vue' /* webpackChunkName: "components/filter-mis-hari" */).then(c => c.default || c)
export const LazyFilterMisJenisSchema = import('../../components/filter/mis/jenis-schema.vue' /* webpackChunkName: "components/filter-mis-jenis-schema" */).then(c => c.default || c)
export const LazyFilterMisJurusan = import('../../components/filter/mis/jurusan.vue' /* webpackChunkName: "components/filter-mis-jurusan" */).then(c => c.default || c)
export const LazyFilterMisProgram = import('../../components/filter/mis/program.vue' /* webpackChunkName: "components/filter-mis-program" */).then(c => c.default || c)
export const LazyKuliahMateriCardMateri = import('../../components/kuliah/materi/card-materi.vue' /* webpackChunkName: "components/kuliah-materi-card-materi" */).then(c => c.default || c)
export const LazyKuliahMateriDialogLihatMateri = import('../../components/kuliah/materi/dialog-lihat-materi.vue' /* webpackChunkName: "components/kuliah-materi-dialog-lihat-materi" */).then(c => c.default || c)
export const LazyKuliahMateriDialogUploadMateriBaru = import('../../components/kuliah/materi/dialog-upload-materi-baru.vue' /* webpackChunkName: "components/kuliah-materi-dialog-upload-materi-baru" */).then(c => c.default || c)
export const LazyKuliahMateriDialogUploadMateriSebelumnya = import('../../components/kuliah/materi/dialog-upload-materi-sebelumnya.vue' /* webpackChunkName: "components/kuliah-materi-dialog-upload-materi-sebelumnya" */).then(c => c.default || c)
export const LazyKuliahPengumumanCardPengumuman = import('../../components/kuliah/pengumuman/card-pengumuman.vue' /* webpackChunkName: "components/kuliah-pengumuman-card-pengumuman" */).then(c => c.default || c)
export const LazyKuliahPengumumanCardSemuaPengumuman = import('../../components/kuliah/pengumuman/card-semua-pengumuman.vue' /* webpackChunkName: "components/kuliah-pengumuman-card-semua-pengumuman" */).then(c => c.default || c)
export const LazyKuliahPengumumanDialog = import('../../components/kuliah/pengumuman/dialog.vue' /* webpackChunkName: "components/kuliah-pengumuman-dialog" */).then(c => c.default || c)
export const LazyKuliahTugasCardTugas = import('../../components/kuliah/tugas/card-tugas.vue' /* webpackChunkName: "components/kuliah-tugas-card-tugas" */).then(c => c.default || c)
export const LazyKuliahTugasDialogDetailTugasDosen = import('../../components/kuliah/tugas/dialog-detail-tugas-dosen.vue' /* webpackChunkName: "components/kuliah-tugas-dialog-detail-tugas-dosen" */).then(c => c.default || c)
export const LazyKuliahTugasDialogDetailTugasMahasiswa = import('../../components/kuliah/tugas/dialog-detail-tugas-mahasiswa.vue' /* webpackChunkName: "components/kuliah-tugas-dialog-detail-tugas-mahasiswa" */).then(c => c.default || c)
export const LazyKuliahTugasDialogFormTugas = import('../../components/kuliah/tugas/dialog-form-tugas.vue' /* webpackChunkName: "components/kuliah-tugas-dialog-form-tugas" */).then(c => c.default || c)
export const LazyKuliahTugasDialogSubmitNilai = import('../../components/kuliah/tugas/dialog-submit-nilai.vue' /* webpackChunkName: "components/kuliah-tugas-dialog-submit-nilai" */).then(c => c.default || c)
export const LazyKuliahTugasDialogSubmitTugas = import('../../components/kuliah/tugas/dialog-submit-tugas.vue' /* webpackChunkName: "components/kuliah-tugas-dialog-submit-tugas" */).then(c => c.default || c)
export const LazyKuliahVideoCardVideo = import('../../components/kuliah/video/card-video.vue' /* webpackChunkName: "components/kuliah-video-card-video" */).then(c => c.default || c)
export const LazyKuliahVideoDialogLihatVideo = import('../../components/kuliah/video/dialog-lihat-video.vue' /* webpackChunkName: "components/kuliah-video-dialog-lihat-video" */).then(c => c.default || c)
export const LazyKuliahVideoDialog = import('../../components/kuliah/video/dialog.vue' /* webpackChunkName: "components/kuliah-video-dialog" */).then(c => c.default || c)
export const LazyMasterRoomConferenceDialog = import('../../components/master/room-conference/dialog.vue' /* webpackChunkName: "components/master-room-conference-dialog" */).then(c => c.default || c)
export const LazyMasterServerConferenceDialog = import('../../components/master/server-conference/dialog.vue' /* webpackChunkName: "components/master-server-conference-dialog" */).then(c => c.default || c)
export const LazyMenuHakAdmin = import('../../components/menu/hak/admin.vue' /* webpackChunkName: "components/menu-hak-admin" */).then(c => c.default || c)
export const LazyMenuHakBaak = import('../../components/menu/hak/baak.vue' /* webpackChunkName: "components/menu-hak-baak" */).then(c => c.default || c)
export const LazyMenuHakDosen = import('../../components/menu/hak/dosen.vue' /* webpackChunkName: "components/menu-hak-dosen" */).then(c => c.default || c)
export const LazyMenuHakKaprodi = import('../../components/menu/hak/kaprodi.vue' /* webpackChunkName: "components/menu-hak-kaprodi" */).then(c => c.default || c)
export const LazyMenuHakMahasiswa = import('../../components/menu/hak/mahasiswa.vue' /* webpackChunkName: "components/menu-hak-mahasiswa" */).then(c => c.default || c)
export const LazyPengaturanTanggalLiburDialog = import('../../components/pengaturan/tanggal-libur/dialog.vue' /* webpackChunkName: "components/pengaturan-tanggal-libur-dialog" */).then(c => c.default || c)
