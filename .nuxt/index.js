import Vue from 'vue'
import Vuex from 'vuex'
import Meta from 'vue-meta'
import ClientOnly from 'vue-client-only'
import NoSsr from 'vue-no-ssr'
import { createRouter } from './router.js'
import NuxtChild from './components/nuxt-child.js'
import NuxtError from '../layouts/error.vue'
import Nuxt from './components/nuxt.js'
import App from './App.js'
import { setContext, getLocation, getRouteData, normalizeError } from './utils'
import { createStore } from './store.js'

/* Plugins */

import nuxt_plugin_plugin_41f10b71 from 'nuxt_plugin_plugin_41f10b71' // Source: ./components/plugin.js (mode: 'all')
import nuxt_plugin_plugin_6b8a561d from 'nuxt_plugin_plugin_6b8a561d' // Source: ./vuetify/plugin.js (mode: 'all')
import nuxt_plugin_clipboard_42079fe7 from 'nuxt_plugin_clipboard_42079fe7' // Source: ./clipboard.js (mode: 'client')
import nuxt_plugin_nuxtuseragent_2a3a6610 from 'nuxt_plugin_nuxtuseragent_2a3a6610' // Source: ./nuxt-user-agent.js (mode: 'all')
import nuxt_plugin_index_2bae3767 from 'nuxt_plugin_index_2bae3767' // Source: ./firebase/index.js (mode: 'all')
import nuxt_plugin_workbox_6e66a763 from 'nuxt_plugin_workbox_6e66a763' // Source: ./workbox.js (mode: 'client')
import nuxt_plugin_metaplugin_649fdb76 from 'nuxt_plugin_metaplugin_649fdb76' // Source: ./pwa/meta.plugin.js (mode: 'all')
import nuxt_plugin_iconplugin_694780ea from 'nuxt_plugin_iconplugin_694780ea' // Source: ./pwa/icon.plugin.js (mode: 'all')
import nuxt_plugin_axios_6987cff2 from 'nuxt_plugin_axios_6987cff2' // Source: ./axios.js (mode: 'all')
import nuxt_plugin_axios_3566aa80 from 'nuxt_plugin_axios_3566aa80' // Source: ../plugins/axios (mode: 'all')
import nuxt_plugin_vuecarousel_e4202ecc from 'nuxt_plugin_vuecarousel_e4202ecc' // Source: ../plugins/vue-carousel.js (mode: 'client')
import nuxt_plugin_persistedState_1da1c1a3 from 'nuxt_plugin_persistedState_1da1c1a3' // Source: ../plugins/persistedState.js (mode: 'client')
import nuxt_plugin_vuevideo_9f168b0e from 'nuxt_plugin_vuevideo_9f168b0e' // Source: ../plugins/vue-video.js (mode: 'client')
import nuxt_plugin_socket_4695eaee from 'nuxt_plugin_socket_4695eaee' // Source: ../plugins/socket.js (mode: 'client')
import nuxt_plugin_TiptapVuetify_5e6e5270 from 'nuxt_plugin_TiptapVuetify_5e6e5270' // Source: ../plugins/TiptapVuetify (mode: 'client')

// Component: <ClientOnly>
Vue.component(ClientOnly.name, ClientOnly)

// TODO: Remove in Nuxt 3: <NoSsr>
Vue.component(NoSsr.name, {
  ...NoSsr,
  render (h, ctx) {
    if (process.client && !NoSsr._warned) {
      NoSsr._warned = true

      console.warn('<no-ssr> has been deprecated and will be removed in Nuxt 3, please use <client-only> instead')
    }
    return NoSsr.render(h, ctx)
  }
})

// Component: <NuxtChild>
Vue.component(NuxtChild.name, NuxtChild)
Vue.component('NChild', NuxtChild)

// Component NuxtLink is imported in server.js or client.js

// Component: <Nuxt>
Vue.component(Nuxt.name, Nuxt)

Object.defineProperty(Vue.prototype, '$nuxt', {
  get() {
    return this.$root.$options.$nuxt
  },
  configurable: true
})

Vue.use(Meta, {"keyName":"head","attribute":"data-n-head","ssrAttribute":"data-n-head-ssr","tagIDKeyName":"hid"})

const defaultTransition = {"name":"page","mode":"out-in","appear":false,"appearClass":"appear","appearActiveClass":"appear-active","appearToClass":"appear-to"}

const originalRegisterModule = Vuex.Store.prototype.registerModule

function registerModule (path, rawModule, options = {}) {
  const preserveState = process.client && (
    Array.isArray(path)
      ? !!path.reduce((namespacedState, path) => namespacedState && namespacedState[path], this.state)
      : path in this.state
  )
  return originalRegisterModule.call(this, path, rawModule, { preserveState, ...options })
}

async function createApp(ssrContext, config = {}) {
  const router = await createRouter(ssrContext, config)

  const store = createStore(ssrContext)
  // Add this.$router into store actions/mutations
  store.$router = router

  // Fix SSR caveat https://github.com/nuxt/nuxt.js/issues/3757#issuecomment-414689141
  store.registerModule = registerModule

  // Create Root instance

  // here we inject the router and store to all child components,
  // making them available everywhere as `this.$router` and `this.$store`.
  const app = {
    head: {"titleTemplate":"%s - ETHOL","title":"Enterprise Technology Hybrid Online Learning","meta":[{"charset":"utf-8"},{"name":"viewport","content":"width=device-width, initial-scale=1"},{"hid":"description","name":"description","content":""}],"link":[{"rel":"icon","type":"image\u002Fx-icon","href":"\u002Ficon.png"},{"rel":"stylesheet","type":"text\u002Fcss","href":"https:\u002F\u002Ffonts.googleapis.com\u002Fcss?family=Roboto:100,300,400,500,700,900&display=swap"},{"rel":"stylesheet","type":"text\u002Fcss","href":"https:\u002F\u002Fcdn.jsdelivr.net\u002Fnpm\u002F@mdi\u002Ffont@latest\u002Fcss\u002Fmaterialdesignicons.min.css"}],"style":[],"script":[]},

    store,
    router,
    nuxt: {
      defaultTransition,
      transitions: [defaultTransition],
      setTransitions (transitions) {
        if (!Array.isArray(transitions)) {
          transitions = [transitions]
        }
        transitions = transitions.map((transition) => {
          if (!transition) {
            transition = defaultTransition
          } else if (typeof transition === 'string') {
            transition = Object.assign({}, defaultTransition, { name: transition })
          } else {
            transition = Object.assign({}, defaultTransition, transition)
          }
          return transition
        })
        this.$options.nuxt.transitions = transitions
        return transitions
      },

      err: null,
      dateErr: null,
      error (err) {
        err = err || null
        app.context._errored = Boolean(err)
        err = err ? normalizeError(err) : null
        let nuxt = app.nuxt // to work with @vue/composition-api, see https://github.com/nuxt/nuxt.js/issues/6517#issuecomment-573280207
        if (this) {
          nuxt = this.nuxt || this.$options.nuxt
        }
        nuxt.dateErr = Date.now()
        nuxt.err = err
        // Used in src/server.js
        if (ssrContext) {
          ssrContext.nuxt.error = err
        }
        return err
      }
    },
    ...App
  }

  // Make app available into store via this.app
  store.app = app

  const next = ssrContext ? ssrContext.next : location => app.router.push(location)
  // Resolve route
  let route
  if (ssrContext) {
    route = router.resolve(ssrContext.url).route
  } else {
    const path = getLocation(router.options.base, router.options.mode)
    route = router.resolve(path).route
  }

  // Set context to app.context
  await setContext(app, {
    store,
    route,
    next,
    error: app.nuxt.error.bind(app),
    payload: ssrContext ? ssrContext.payload : undefined,
    req: ssrContext ? ssrContext.req : undefined,
    res: ssrContext ? ssrContext.res : undefined,
    beforeRenderFns: ssrContext ? ssrContext.beforeRenderFns : undefined,
    ssrContext
  })

  function inject(key, value) {
    if (!key) {
      throw new Error('inject(key, value) has no key provided')
    }
    if (value === undefined) {
      throw new Error(`inject('${key}', value) has no value provided`)
    }

    key = '$' + key
    // Add into app
    app[key] = value
    // Add into context
    if (!app.context[key]) {
      app.context[key] = value
    }

    // Add into store
    store[key] = app[key]

    // Check if plugin not already installed
    const installKey = '__nuxt_' + key + '_installed__'
    if (Vue[installKey]) {
      return
    }
    Vue[installKey] = true
    // Call Vue.use() to install the plugin into vm
    Vue.use(() => {
      if (!Object.prototype.hasOwnProperty.call(Vue.prototype, key)) {
        Object.defineProperty(Vue.prototype, key, {
          get () {
            return this.$root.$options[key]
          }
        })
      }
    })
  }

  // Inject runtime config as $config
  inject('config', config)

  if (process.client) {
    // Replace store state before plugins execution
    if (window.__NUXT__ && window.__NUXT__.state) {
      store.replaceState(window.__NUXT__.state)
    }
  }

  // Add enablePreview(previewData = {}) in context for plugins
  if (process.static && process.client) {
    app.context.enablePreview = function (previewData = {}) {
      app.previewData = Object.assign({}, previewData)
      inject('preview', previewData)
    }
  }
  // Plugin execution

  if (typeof nuxt_plugin_plugin_41f10b71 === 'function') {
    await nuxt_plugin_plugin_41f10b71(app.context, inject)
  }

  if (typeof nuxt_plugin_plugin_6b8a561d === 'function') {
    await nuxt_plugin_plugin_6b8a561d(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_clipboard_42079fe7 === 'function') {
    await nuxt_plugin_clipboard_42079fe7(app.context, inject)
  }

  if (typeof nuxt_plugin_nuxtuseragent_2a3a6610 === 'function') {
    await nuxt_plugin_nuxtuseragent_2a3a6610(app.context, inject)
  }

  if (typeof nuxt_plugin_index_2bae3767 === 'function') {
    await nuxt_plugin_index_2bae3767(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_workbox_6e66a763 === 'function') {
    await nuxt_plugin_workbox_6e66a763(app.context, inject)
  }

  if (typeof nuxt_plugin_metaplugin_649fdb76 === 'function') {
    await nuxt_plugin_metaplugin_649fdb76(app.context, inject)
  }

  if (typeof nuxt_plugin_iconplugin_694780ea === 'function') {
    await nuxt_plugin_iconplugin_694780ea(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_6987cff2 === 'function') {
    await nuxt_plugin_axios_6987cff2(app.context, inject)
  }

  if (typeof nuxt_plugin_axios_3566aa80 === 'function') {
    await nuxt_plugin_axios_3566aa80(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vuecarousel_e4202ecc === 'function') {
    await nuxt_plugin_vuecarousel_e4202ecc(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_persistedState_1da1c1a3 === 'function') {
    await nuxt_plugin_persistedState_1da1c1a3(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_vuevideo_9f168b0e === 'function') {
    await nuxt_plugin_vuevideo_9f168b0e(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_socket_4695eaee === 'function') {
    await nuxt_plugin_socket_4695eaee(app.context, inject)
  }

  if (process.client && typeof nuxt_plugin_TiptapVuetify_5e6e5270 === 'function') {
    await nuxt_plugin_TiptapVuetify_5e6e5270(app.context, inject)
  }

  // Lock enablePreview in context
  if (process.static && process.client) {
    app.context.enablePreview = function () {
      console.warn('You cannot call enablePreview() outside a plugin.')
    }
  }

  // If server-side, wait for async component to be resolved first
  if (process.server && ssrContext && ssrContext.url) {
    await new Promise((resolve, reject) => {
      router.push(ssrContext.url, resolve, (err) => {
        // https://github.com/vuejs/vue-router/blob/v3.4.3/src/util/errors.js
        if (!err._isRouter) return reject(err)
        if (err.type !== 2 /* NavigationFailureType.redirected */) return resolve()

        // navigated to a different route in router guard
        const unregister = router.afterEach(async (to, from) => {
          ssrContext.url = to.fullPath
          app.context.route = await getRouteData(to)
          app.context.params = to.params || {}
          app.context.query = to.query || {}
          unregister()
          resolve()
        })
      })
    })
  }

  return {
    store,
    app,
    router
  }
}

export { createApp, NuxtError }
